from tkinter import *
import inside as ins
import wyjatki as w


class tkinter(object):
    expression = ''
    wrzucone = '0'
    cena = '0'
    lista = []
    number = '0'
    roznica = '0'
    d = 0

    def __init__(self):
        expression = self.expression
        wrzucone = self.wrzucone
        cena = self.cena
        lista = self.lista
        roznica = self.roznica
        self.d = ins.Obsluga(self.number)
        self.g = ins.Monety()

    def przycisk(self, num):
        self.expression = self.expression + str(num)
        string.set(self.expression)

    def moneta(self, num):
        self.wrzucone = self.wrzucone + "+" + str(num)
        self.wrzucone = str(round(eval(self.wrzucone), 3))
        string.set(self.wrzucone)
        self.lista.append(num)

    def obl_roznice(self):
        self.roznica = self.wrzucone + "-" + self.cena
        self.roznica = str(round(eval(self.roznica), 3))
        return self.roznica

    # ---------------------PRZYCISKI FUNKCYJNE -----------------------

    def okpress(self):
        self.number = eval(self.expression)
        self.d = ins.Obsluga(self.number)
        if self.d.check() == 1:
            self.cena = str(self.d.Podaj_cene(self.number))
            self.expression = "Cena wybranego produktu to " + self.cena + " zł. \nWrzuć monety."
            string.set(self.expression)
            self.expression = ""
        else:
            self.expression = "Nie mogę sprzedać tego produktu.\nSprawdź czy jest on dostępny \nlub czy wybrałeś dobry numer."
            string.set(self.expression)
            raise w.BladNumeruLubBrakProduktu

    def checkpress(self):
        self.number = eval(self.expression)
        self.d = ins.Obsluga(self.number)

        if self.d.check() == 0:
            self.expression = "Podałeś zły numer produktu, \nkliknij czerwony przycisk i spróbuj jescze raz."
            string.set(self.expression)
            raise w.BlednyNumer()
        elif self.d.check() == 1:
            self.expression = "Produkt jest dostępny.\n"
            string.set(self.expression)
            self.cena = str(self.d.Podaj_cene(self.number))
            self.expression = self.expression + "Cena wybranego produktu to " + self.cena + " zł."
            string.set(self.expression)
            self.expression = ""
        elif self.d.check() == 2:
            self.expression = "Produkt jest niedostępny."
            string.set(self.expression)
            raise w.ProduktWyprzedany()

    def clearpress(self):
        self.expression = ""
        string.set(self.expression)
        if self.wrzucone is not "0":
            string.set("Zwracam wrzucone pieniądze.")
            self.wrzucone = "0"
        else:
            string.set(
                'Podaj numer (30-50) a następnie co chcesz zrobić \n Zielony przycisk - rozpoczecie zakupu\n Czerwony - anulowanie zakupu, \n Niebieski - sprawdzenie dostepnosci towaru')

    def donepress(self):
        if float(self.wrzucone) < float(self.cena):
            brak = self.cena + "-" + self.wrzucone
            brak = str(round(eval(brak), 3))
            self.expression = "Do zapłaty pozostało jeszcze " + brak
            string.set(self.expression)

        if float(self.wrzucone) >= float(self.cena):
            self.expression = 'Wydawanie produktu.'
            string.set(self.expression)
            roz = tkinter.obl_roznice(self)
            k = self.d.reszta(roz)

            string.set(self.expression + k[0])

            if k[1] == False:
                self.wrzucone = '0'

            self.d.usun_asortyment()

            for i in range(len(self.lista)):
                self.g.add_to_monety(self.lista[i])
            print("Dodano monety.")
            self.lista = []
            self.wrzucone = '0'


tk = tkinter()
window = Tk()
window.title("Automat z napojami.")

HEIGHT = 500
WIDTH = 1200

canva = Canvas(window, height=HEIGHT, width=WIDTH)
canva.pack()

# *************IKONKI**************

picture_1 = PhotoImage(file="img/bgg.png")
ok = PhotoImage(file="img/ok.png")
cancel = PhotoImage(file="img/cancel1.png")
jeden_g = PhotoImage(file="img/1gg.png")
dwa_g = PhotoImage(file="img/2gg.png")
piec_g = PhotoImage(file="img/5gg.png")
d_g = PhotoImage(file="img/10gg.png")
dd_g = PhotoImage(file="img/20gg.png")
pd_g = PhotoImage(file="img/50gg.png")
jeden_z = PhotoImage(file="img/1z.png")
dwa_z = PhotoImage(file="img/2z.png")
piec_z = PhotoImage(file="img/5z.png")

string = StringVar()
string.set(
    'Podaj numer (30-50) a następnie co chcesz zrobić \n Zielony przycisk - rozpoczecie zakupu\n Czerwony - anulowanie zakupu, \n Niebieski - sprawdzenie dostepnosci towaru')

# *************1 label************

label_1 = Label(window, image=picture_1)
label_1.place(relwidth=1, relheight=1)

# *****************RAMKI************

frame_1 = Frame(window, width=500, height=500, cursor="dot", bg="white")
frame_1.place(relx=0.05, rely=0.25, relwidth=0.5, relheight=0.5)
frame_2 = Frame(window, width=500, height=500, cursor="dot", bg="black")
frame_2.place(relx=0.6, rely=0, relwidth=0.4, relheight=1)

# ****************LABELE************

label_2 = Label(frame_1, bg="black", bd=8, relief="sunken", fg="white", font="courier 15", textvariable=string,
                anchor=CENTER)
label_2.place(relx=0, rely=0, relwidth=1, relheight=1)

# *******************DEKLARACJE PRZYCISKOW*******************************

button_1 = Button(frame_2, text="1", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(1))
button_2 = Button(frame_2, text="2", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(2))
button_3 = Button(frame_2, text="3", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(3))
button_4 = Button(frame_2, text="4", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(4))
button_5 = Button(frame_2, text="5", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(5))
button_6 = Button(frame_2, text="6", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(6))
button_7 = Button(frame_2, text="7", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(7))
button_8 = Button(frame_2, text="8", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(8))
button_9 = Button(frame_2, text="9", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(9))
button_0 = Button(frame_2, text="0", font=40, bd=0, activebackground="grey", command=lambda: tk.przycisk(0))

button_ok = Button(frame_2, image=ok, bg="black", bd=0, command=tk.okpress)
button_cancel = Button(frame_2, image=cancel, bg="black", bd=0, command=tk.clearpress)

button_5z = Button(frame_2, text="5zł", font=40, bd=0, image=piec_z, bg="black", activebackground="black",
                   command=lambda: tk.moneta(5.0))
button_2z = Button(frame_2, text="2zł", font=40, bd=0, image=dwa_z, bg="black", activebackground="black",
                   command=lambda: tk.moneta(2.0))
button_1z = Button(frame_2, text="1zł", font=40, bd=0, image=jeden_z, bg="black", activebackground="black",
                   command=lambda: tk.moneta(1.0))
button_50g = Button(frame_2, text="50gr", font=40, bd=0, image=pd_g, bg="black", activebackground="black",
                    command=lambda: tk.moneta(0.5))
button_20g = Button(frame_2, text="20gr", font=40, bd=0, image=dd_g, bg="black", activebackground="black",
                    command=lambda: tk.moneta(0.2))
button_10g = Button(frame_2, text="10gr", font=40, bd=0, image=d_g, bg="black", activebackground="black",
                    command=lambda: tk.moneta(0.1))
button_5g = Button(frame_2, text="5gr", font=40, bd=0, image=piec_g, bg="black", activebackground="black",
                   command=lambda: tk.moneta(0.05))
button_2g = Button(frame_2, text="2gr", font=40, bd=0, image=dwa_g, bg="black", activebackground="black",
                   command=lambda: tk.moneta(0.02))
button_1g = Button(frame_2, text="1gr", font=40, bd=0, image=jeden_g, bg="black", activebackground="black",
                   command=lambda: tk.moneta(0.01))

button_check = Button(frame_2, text="CHECK", font=40, bd=0, bg="navy", fg="white", command=tk.checkpress)
button_done = Button(frame_2, text="DONE", font=40, bd=0, bg="green", fg="white", command=tk.donepress)
# *********************WRZUCANIE PRZYCISKOW **********************

button_1.place(relx=0.1, rely=0.1, relwidth=0.1, relheight=0.1)
button_2.place(relx=0.3, rely=0.1, relwidth=0.1, relheight=0.1)
button_3.place(relx=0.5, rely=0.1, relwidth=0.1, relheight=0.1)
button_4.place(relx=0.1, rely=0.3, relwidth=0.1, relheight=0.1)
button_5.place(relx=0.3, rely=0.3, relwidth=0.1, relheight=0.1)
button_6.place(relx=0.5, rely=0.3, relwidth=0.1, relheight=0.1)
button_7.place(relx=0.1, rely=0.5, relwidth=0.1, relheight=0.1)
button_8.place(relx=0.3, rely=0.5, relwidth=0.1, relheight=0.1)
button_9.place(relx=0.5, rely=0.5, relwidth=0.1, relheight=0.1)
button_ok.place(relx=0.1, rely=0.7, relwidth=0.1, relheight=0.1)
button_0.place(relx=0.3, rely=0.7, relwidth=0.1, relheight=0.1)
button_cancel.place(relx=0.5, rely=0.7, relwidth=0.1, relheight=0.1)
button_check.place(relx=0.08, rely=0.87, relwidth=0.52, relheight=0.05)
button_done.place(relx=0.66, rely=0.87, relwidth=0.3, relheight=0.05)
button_5z.place(relx=0.84, rely=0.3, relwidth=0.08, relheight=0.08)
button_2z.place(relx=0.84, rely=0.42, relwidth=0.08, relheight=0.08)
button_1z.place(relx=0.84, rely=0.54, relwidth=0.08, relheight=0.08)
button_50g.place(relx=0.7, rely=0.11, relwidth=0.08, relheight=0.08)
button_20g.place(relx=0.7, rely=0.23, relwidth=0.08, relheight=0.08)
button_10g.place(relx=0.7, rely=0.35, relwidth=0.08, relheight=0.08)
button_5g.place(relx=0.7, rely=0.47, relwidth=0.08, relheight=0.08)
button_2g.place(relx=0.7, rely=0.59, relwidth=0.08, relheight=0.08)
button_1g.place(relx=0.7, rely=0.71, relwidth=0.08, relheight=0.08)

window.mainloop()

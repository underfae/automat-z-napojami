import random as r
import wyjatki as w


class Monety(object):
    dost_monety = []
    nominaly = [5, 2, 1, 0.2, 0.1, 0.05, 0.02, 0.01]

    for i in range(30):
        x = r.choice(nominaly)
        dost_monety.append(x)
    dost_monety.sort(reverse=True)
    print(dost_monety)

    def __init__(self):
        dost_monety = self.dost_monety

    def add_to_monety(self, moneta):
        self.dost_monety.append(moneta)
        print(self.dost_monety)

    def take_from_monety(self, nominal):
        self.dost_monety.remove(nominal)


class Produkty(object):
    asortyment = [[30, 4.0, 5], [31, 3.20, 5], [32, 1.50, 5], [33, 2.0, 5], [34, 1.55, 5],
                  [35, 4.40, 5], [36, 2.0, 5], [37, 2.20, 5], [38, 1.20, 5], [39, 1.0, 5],
                  [40, 3.0, 5], [41, 3.25, 5], [42, 4.20, 5], [43, 1.60, 5], [44, 1.20, 1],
                  [45, 6.52, 5], [46, 5.0, 5], [47, 2.35, 5], [48, 3.25, 5], [49, 1.60, 5], [50, 8.40, 5]]

    def Podaj_cene(self, number):
        for x in range(len(self.asortyment)):
            if self.asortyment[x][0] == number:
                return self.asortyment[x][1]

    def Podaj_ilosc(self, number):
        for x in range(len(self.asortyment)):
            if self.asortyment[x][0] == number:
                return self.asortyment[x][2]


class Obsluga(Produkty, Monety):

    def __init__(self, number):
        self.number = number

    def check(self):
        if self.number not in range(30, 51):
            ret = 0
        else:
            x = Produkty.Podaj_ilosc(self, self.number)
            if x != 0:
                ret = 1
            else:
                ret = 2
        return ret

    def usun_asortyment(self):
        for x in range(len(self.asortyment)):
            if self.asortyment[x][0] == self.number:
                self.asortyment[x][2] = self.asortyment[x][2] - 1
        print("Napój usuniety")

    def reszta(self, roznica):

        roznica = round(float(roznica), 3)
        i = 0
        pom = []
        dost_ilosc = sum(self.dost_monety)
        expression = ""
        w = True

        if roznica > dost_ilosc:
            expression = "Tylko odliczona kwota. "
            return expression

        else:
            while roznica > 0:
                if i < len(self.dost_monety):
                    if roznica >= self.dost_monety[i]:
                        roznica = round((roznica - self.dost_monety[i]), 3)
                        pom.append(self.dost_monety[i])
                        print(roznica)
                else:
                    expression = "Tylko odliczona kwota."
                    pom = []
                    break
                i = i + 1

            for i in range(len(pom)):
                Monety.take_from_monety(self, pom[i])
                w = False

            print()
            print(self.dost_monety)
            expression = expression + "\n Reszta =  " + str(round(sum(pom), 3)) + " zł"

            return expression, w
